@extends('layout.default')

@section('content')
    <div class="row">
        <div class="card col-12">
            <div class="card-header">
                Basic Information
            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="text" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Phone</label>
                        <input type="text" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Addres</label>
                        <input type="text" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Description</label>
                        <input type="text" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label for="content" class="form-label">Keywords</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="card col-12">
            <div class="card-header">
                Prefences
            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label class="form-label">Disqus URL</label>
                        <input type="url" class="form-control" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Timezone</label>
                        <select name="timezone" id="timezone" class="form-control">
                            <option value="Asia/Jakarta">Asia/Jakarta</option>
                            <option value="America/New_York">America/New_York</option>
                            <option value="Europe/London">Europe/London</option>
                            <option value="Australia/Sydney">Australia/Sydney</option>
                            <option value="Africa/Cairo">Africa/Cairo</option>
                        </select>
                    </div>
                    <div class="form-check form-switch mb-3">
                        <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Use Clean URL</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="card col-12">
            <div class="card-header">
                SMTP Settings

            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label for="smtpServer" class="form-label">SMTP Server</label>
                        <input type="text" class="form-control" id="smtpServer" placeholder="mail.aktual.com">
                    </div>
                    <div class="mb-3">
                        <label for="smtpUsername" class="form-label">SMTP Username</label>
                        <input type="text" class="form-control" id="smtpUsername" placeholder="admin@aktual.com">
                    </div>
                    <div class="mb-3">
                        <label for="smtpPassword" class="form-label">SMTP Password</label>
                        <input type="password" class="form-control" id="smtpPassword" placeholder="••••••••••••">
                    </div>
                    <div class="mb-3">
                        <label for="smtpPort" class="form-label">SMTP Port</label>
                        <input type="number" class="form-control" id="smtpPort" placeholder="587">
                    </div>
                    <div class="mb-3">
                        <label for="securityConnection" class="form-label">Security Connection</label>
                        <select name="securityConnection" id="securityConnection" class="form-control">
                            <option value="TLS">TLS</option>
                            <option value="SSL">SSL</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>

        <div class="card col-12">
            <div class="card-header">
                Website Logo
            </div>
            <div class="card-body">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxBlNEganj7tpMkbNTdc9wZNAxdQXGXYjT2w&usqp=CAU" class="img-fluid mb-3" alt="">
                <form>
                    <div class="mb-3">
                        <label for="filelogo" class="form-label">File Logo</label>
                        <input type="file" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div><div class="card col-12">
            <div class="card-header">
                Website Logo Inverse
            </div>
            <div class="card-body">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxBlNEganj7tpMkbNTdc9wZNAxdQXGXYjT2w&usqp=CAU" class="img-fluid mb-3" alt="">
                <form>
                    <div class="mb-3">
                        <label for="filelogo" class="form-label">File Logo</label>
                        <input type="file" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
        <div class="card col-12">
            <div class="card-header">
                Website Logo Icon
            </div>
            <div class="card-body">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxBlNEganj7tpMkbNTdc9wZNAxdQXGXYjT2w&usqp=CAU" class="img-fluid mb-3" alt="">
                <form>
                    <div class="mb-3">
                        <label for="filelogo" class="form-label">File Logo</label>
                        <input type="file" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
@endsection
