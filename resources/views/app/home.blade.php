@extends('layout.default')

@section('content')
    <div class="row">
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                            <div class="badge bg-success fs-2">
                                <i class="bi bi-eye text-center m-2"></i>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Visible Blog Post</h6>
                            <h6 class="font-extrabold mb-0">112.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                            <div class="badge bg-warning fs-2">
                                <i class="bi bi-eye-slash text-center m-2"></i>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Hidden Blog Post</h6>
                            <h6 class="font-extrabold mb-0">183.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                            <div class="badge bg-success fs-2">
                                <i class="bi bi-file-text text-center m-2"></i>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Published Blog Post</h6>
                            <h6 class="font-extrabold mb-0">80.000</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                            <div class="badge bg-primary fs-2">
                                <i class="bi bi-currency-dollar text-center m-2"></i>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Advertising Banner</h6>
                            <h6 class="font-extrabold mb-0">112</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Weekly Blog Views

                </div>
                <div class="card-body">

                    <canvas id="myChart" style="height: 278px"></canvas>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Audience From
                </div>
                <div class="card-body">
                    <div id="world-map" style="height: 478px"></div>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Recent Blog Post
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Views</th>
                                    <th scope="col">Publish Date</th>
                                    <th scope="col" colspan="2">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tepatkah Penyebutan “Teroris” Bagi KKB Papua Dari Perspektif Hukum Internasional?</td>
                                    <td>Intern Suka Hukum</td>
                                    <td>64</td>
                                    <td>January 24, 2023</td>
                                    <td><div class="badge badge-pill bg-success">Visible</div></td>
                                    <td><div class="btn btn-sm btn-primary">Edit</div></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://sukahukum.com/cpanel/vendors/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="https://sukahukum.com/cpanel/vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>


    <script>
        const ctx = document.getElementById('myChart');

        new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#world-map').vectorMap({
                map: 'world_mill_en', // Menampilkan peta dunia dengan jVectorMap
                backgroundColor: '#ffffff', // Warna latar belakang
                zoomOnScroll: true, // Zoom saat melakukan scroll
                regionStyle: {
                    initial: {
                        fill: '#eeeeee', // Warna wilayah awal
                        'fill-opacity': 1,
                        stroke: 'none',
                        'stroke-width': 0,
                        'stroke-opacity': 1
                    },
                    hover: {
                        'fill-opacity': 0.8, // Opacity saat dihover
                        cursor: 'pointer'
                    }
                },
                series: {
                    regions: [{
                        values: {
                            // Misalnya, atur nilai untuk beberapa negara
                            'US': '#2ecc71', // Warna untuk Amerika Serikat
                            'CA': '#3498db', // Warna untuk Kanada
                            'GB': '#e74c3c', // Warna untuk Britania Raya
                            // ...Tambahkan negara lain di sini
                        },
                        attribute: 'fill' // Attribut yang akan diisi (dalam kasus ini, warna)
                    }]
                },
                onRegionTipShow: function(e, el, code) {
                    // Tampilkan informasi saat dihover
                    el.html(el.html() + ' (Kode Negara: ' + code + ')');
                }
            });
        });
    </script>
@endpush
