@extends('layout.default')
@section('content')
    @push('styles')
        <link rel="stylesheet"
            href="{{ asset('template/assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- SweetAlert2 CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    @endpush
    <div class="row">
        <div class="card">
            <div class="card-header">
                Categories
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-end">
                    <button class="btn btn-info btn-md" data-bs-toggle="modal" data-bs-target="#addModal"> + Add New
                        Category</button>
                </div>
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                            @foreach ($categories as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <div class="d-flex justify-content-center">
                                            <div style="padding-right: 10px;">
                                                <button class="btn btn-info btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#editModal"><i class="fa fa-pencil"
                                                        aria-hidden="true"></i></button>
                                                <div class="modal fade" id="editModal" tabindex="-1"
                                                    aria-labelledby="editModal" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <form action="{{ route('subcategory.update', $item->id) }}"
                                                            method="POST">
                                                            @method('PUT')
                                                            @csrf
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h1 class="modal-title fs-5" id="editModal">Edit
                                                                        Category
                                                                    </h1>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="mb-3">
                                                                        <label for="">Category Name</label>
                                                                        <input type="text" name="name"
                                                                            class="form-control"
                                                                            value="{{ $item->name }}">
                                                                    </div>
                                                                    <div class="mb-3">
                                                                        <button class="btn btn-info btn-md"
                                                                            type="submit">Edit Category</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mr-2">
                                                <form action="{{ route('subcategory.delete', $item->id) }}" method="POST"
                                                    id="delete-form">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm" type="submit"><i
                                                            class="fa fa-trash" aria-hidden="true"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModal" aria-hidden="true">
                <div class="modal-dialog">
                    <form action="{{ route('subcategory.store') }}" method="POST">
                        @method('POST')
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="addModal">Add Category</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="">Category Name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <button class="btn btn-info btn-md" type="submit">Add Category</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
        <script>
            let jquery_datatable = $("#table1").DataTable({
                responsive: true
            })
        </script>
        <!-- SweetAlert2 JS -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                // Select the delete form
                const deleteForm = document.getElementById('delete-form');

                // Attach event listener for form submission
                deleteForm.addEventListener('submit', function(event) {
                    // Prevent the form from submitting
                    event.preventDefault();

                    // Display SweetAlert2 confirmation dialog
                    Swal.fire({
                        title: 'Are you sure?',
                        text: 'You won\'t be able to revert this!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        // If confirmed, submit the form
                        if (result.isConfirmed) {
                            deleteForm.submit();
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
