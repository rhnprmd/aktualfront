@extends('layout.default')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                Publish Article
            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label for="postTitle" class="form-label">Title</label>
                        <input type="text" class="form-control" id="postTitle" placeholder="Enter Post Title">
                    </div>
                    <div class="mb-3">
                        <label for="category" class="form-label">Category</label>
                        <select class="form-select" id="category">
                            <option selected>Select one</option>
                            <option value="category1">Category 1</option>
                            <option value="category2">Category 2</option>
                            <!-- Tambahkan opsi kategori lainnya di sini -->
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="shortDescription" class="form-label">Short Description</label>
                        <textarea class="form-control" id="shortDescription" rows="4"
                            placeholder="Enter Short Description (Minimum 150 Characters)"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="content" class="form-label">Content</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                      </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" class="form-control" id="image">
                    </div>
                    <div class="mb-3">
                        <label for="youtubeVideoID" class="form-label">Youtube Video ID</label>
                        <input type="text" class="form-control" id="youtubeVideoID"
                            placeholder="*Optional* Video Will Be Displayed Instead Of Image">
                    </div>
                    <div class="mb-3">
                        <label for="writerName" class="form-label">Writer Name</label>
                        <input type="text" class="form-control" id="writerName" placeholder="Writer's Name">
                    </div>
                    <div class="mb-3">
                        <label for="writerDetail" class="form-label">Writer Detail</label>
                        <textarea class="form-control" id="writerDetail" rows="4" placeholder="Writer's Detail"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="writerPic" class="form-label">Writer Picture</label>
                        <input type="file" class="form-control" id="writerPic">
                    </div>
                    <div class="mb-3">
                        <label for="tags" class="form-label">Tags</label>
                        <input type="text" class="form-control" id="tags" placeholder="Add Tags">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <!-- Panggil Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script>
        // Inisialisasi Summernote pada textarea dengan id 'content'
        $(document).ready(function() {
            $('#content').summernote({
                height: 300 // Tinggi area teks
            });
        });
    </script>
@endpush
