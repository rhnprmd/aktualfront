@extends('layout.default')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                Create Newsletter
            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label for="postTitle" class="form-label">Subject</label>
                        <input type="text" class="form-control" id="postTitle" placeholder="Enter Newsletter Title">
                    </div>
                    <div class="mb-3">
                        <label for="content" class="form-label">Letter</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                      </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <!-- Panggil Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script>
        // Inisialisasi Summernote pada textarea dengan id 'content'
        $(document).ready(function() {
            $('#content').summernote({
                height: 300 // Tinggi area teks
            });
        });
    </script>
@endpush
