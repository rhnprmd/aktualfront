@extends('layout.default')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                Create Banner
            </div>
            <div class="card-body">
                <form>
                    <div class="mb-3">
                        <label class="form-label">Banner Name</label>
                        <input type="text" class="form-control" placeholder="Enter Banner Title">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Banner Size</label>
                        <input type="text" class="form-control" placeholder="Enter Banner Size">
                    </div>
                    <div class="mb-3">
                        <label for="content" class="form-label">Banner Code</label>
                        <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                      </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
@endsection
