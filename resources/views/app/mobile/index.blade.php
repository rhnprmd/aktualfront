<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aktual Mobile</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
        integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
    body,
    html {
        overflow: hidden;
        /* Disable vertical scrolling */
    }

    body {
        position: relative;
        margin: 0;
        /* Menghapus margin bawaan */
        padding: 0;
        /* Menghapus padding bawaan */
    }

    body::before,
    body::after {
        content: "";
        position: fixed;
        left: 0;
        width: 100%;
        height: 250px;
        /* Tinggi bayangan di bagian atas dan bawah */
        z-index: 1;
        /* Bayangan hitam untuk efek 3D */
    }

    body::before {
        top: 0;
        background-image: linear-gradient(to bottom, rgba(0, 0, 0, 1), rgba(0, 0, 0, 0));
        /* Efek bayangan dari atas */
    }

    body::after {
        bottom: 0;
        background-image: linear-gradient(to top, rgba(0, 0, 0, 1), rgba(0, 0, 0, 0));
        /* Efek bayangan dari bawah */
    }


    .vh-100 {
        height: 100vh;
        overflow-y: hidden;
        /* Hide content overflow */
    }

    .icon-bar {
        position: fixed;
        bottom: 30px;
        right: 10px;
        display: flex;
        flex-direction: column;
        align-items: flex-end;
    }

    .icon-bar div {
        margin-bottom: 3px;
        margin-right: 5px;
        padding: 8px;
        transition: all 0.3s ease;
    }

    .konten {
        z-index: 1000;
        padding-bottom: 30px;
        padding-left: 5px;
    }

    .btn-danger {
        background-color: #780B0B;
    }

    #floatingImage {
        position: relative;
        z-index: 1000;
        /* Pastikan z-index lebih tinggi agar gambar tetap terlihat */
    }
</style>

<body class="vh-100" style="background-color: #151521;">
    <header>
        <div class="offcanvas offcanvas-start" style="background-color: #780B0B; width: 80%;" tabindex="-1" id="offcanvasExample"
            aria-labelledby="offcanvasExampleLabel">
            <div class="offcanvas-header">
                <div class="input-group rounded-pill mb-3">
                    <input type="text" class="form-control " placeholder="Search..." aria-label="Search..."
                        aria-describedby="button-addon2">
                    <button class="btn btn-light" type="button" id="button-addon2">
                        <i class="fa-solid fa-magnifying-glass"></i>
                    </button>
                </div>
            </div>
            <div class="offcanvas-body text-white">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                    <a class="nav-link" href="#">Features</a>
                    <a class="nav-link" href="#">Pricing</a>
                    <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                </div>
            </div>
        </div>


        <img src="https://s3-alpha-sig.figma.com/img/b957/b874/b4d8ee5a362ffb3dc6ff383ce9b6628d?Expires=1702857600&Signature=HojehfSpP0EEN95SDLMxTA-8mQFvffpmUMXliYWb0euAASuwqvtZnrbK8CdUk7Ef1S~a7HvvvtAF2eS7~AystcduMuKVvxgrEMYxJQe-N~uSOlMolWTtB2aDb3uzXqI-2~8Ze5GQSnSflWm4wRS4IxslOzfLQ9nmj073fCZA7DprNqOffKm3o2C2cY69TKCG45~A4Cq-j~egvQHQ9QjAdkUujyuaQtqBKjBgOYgn388ootgc3~W3pgDLwYg~l70GhckFJjuNLN2ArZPoWbLP3wE1IzymwOug1N28dawl2F3HxuuNRxV4m-TBToOXqpmA0dF75Lwt8bDRUCaJHbOB5Q__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
            alt="" style="position: fixed; top: 0; left: 50%; transform: translateX(-50%);" width="100"
            class="mt-4" id="floatingImage">
    </header>

    <div class="icon-bar text-white" style="z-index: 1100;">
        <div><i class="fas fa-home btn btn-light btn-lg rounded-circle p-2"></i></div>
        <div data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample"><i
                class="fas fa-share btn btn-light btn-lg rounded-circle p-2"></i></div>
        <div id="scrollUpBtn"><i class="fas fa-chevron-up btn btn-light btn-lg rounded-circle p-2"></i></div>
        <div id="scrollDownBtn"><i class="fas fa-chevron-down btn btn-light btn-lg rounded-circle p-2"></i></div>
    </div>

    <section class="vh-100 bg-success d-flex align-items-end" id="section1"
    style="background-image: url('https://s3-alpha-sig.figma.com/img/18b6/6172/84bfda758199db786861f36f39598d35?Expires=1703462400&Signature=TAH9-JoyUqkTp~rktO~aUYgF3PQE7Et78mE2WnyMjiHNY6x-pxiIvjiP4m8GGV6G6EpBtvwFsnBvCSFG6W~9cmyDlJ6DBNLI~tFB~EIHZtFsacyAn8qIrJR7TlwJT7eG2tXIg0Go9f7yesFK0RxWOw3hWhhawC2T8nu2LsoKk7Aozk5wFAGyGirYwrjdP-v-JHh0LSZGoDuEXskkwEJAL~Q1-~KWUN3diq~gf96N65Hft8m3aWG0ZDJ2podwB0KGR2zuUpE-e0-4jT19bq-QKQG0a~ygs40Z4IBK1MLt31OVbHsWqYfPnsHe1-ah~clOtrsyr7AmrtCfp6Ifs4mZrg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4');            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
        <div class="text-white konten m-2">
            <div class="col-9">
                <h1>Halo</h1>
                <p>Apakah bapanya mirna segera akan meninggalkan indonesia setelah semuanya yang telah terjadi?</p>
                <a class="btn btn-danger rounded-pill col-12 mb-5">Baca Selengkapnya <img src="https://s3-alpha-sig.figma.com/img/ffb7/a2f9/1f28358111a9973fabddcbb06e546b96?Expires=1703462400&Signature=l3dCybcMuVfwDCNZaTxbtLZqFFH3Xf5TCEEBFTTSjAbT02aUmVwXPZ1r70RZo6ri2XVEqVDrx1z45KMcoVuFWsU2NJNLTNycls0i8eRd2aZV9j2saSBCHf-EnSK5y6j0HIxO29J4qSeCqj48P7IgtICcksP-SaIUGRSNc-CGglKnY75~D3sVAGkEHiImGUsfubjirQn2WGIQs77vhnToLO-MzkHCA~d7862DW48RA81hDpwXf7eCxmd-w7JzTGl3c25-KfYrXm~Wmd1jaXMbM~qnXnFkgjs~KeA7lX4q~EK37FkO6~sXFHxaB4WRNv8gEvTvjCAUIlXI1BchGZ4ZYA__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" alt="" width="20" class="mx-2"></a>
            </div>
        </div>
    </section>

    <section class="vh-100 bg-danger" id="section2"
    style="background-image: url('https://images.pexels.com/photos/433989/pexels-photo-433989.jpeg');            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
    </section>

    <section class="vh-100 bg-warning" id="section3"
    style="background-image: url('https://plus.unsplash.com/premium_photo-1674664384602-a3d583f4547b?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8dmVydGljYWx8ZW58MHx8MHx8fDA%3D');            background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/js/all.min.js"
        integrity="sha512-GWzVrcGlo0TxTRvz9ttioyYJ+Wwk9Ck0G81D+eO63BaqHaJ3YZX9wuqjwgfcV/MrB2PhaVX9DkYVhbFpStnqpQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            const sections = document.querySelectorAll('section');

            function scrollToNextSection() {
                const nextSection = sections[getCurrentSectionIndex() + 1];
                if (nextSection) {
                    nextSection.scrollIntoView({
                        behavior: 'smooth',
                        block: 'start'
                    });
                }
            }

            function scrollToPreviousSection() {
                const prevSection = sections[getCurrentSectionIndex() - 1];
                if (prevSection) {
                    prevSection.scrollIntoView({
                        behavior: 'smooth',
                        block: 'start'
                    });
                }
            }

            function getCurrentSectionIndex() {
                const currentScroll = window.scrollY;
                return [...sections].findIndex(section => currentScroll >= section.offsetTop && currentScroll <
                    section.offsetTop + section.offsetHeight);
            }

            document.getElementById('scrollUpBtn').addEventListener('click', function() {
                scrollToPreviousSection();
                console.log('Atas')
            });

            // Menambahkan event listener untuk tombol scroll down
            document.getElementById('scrollDownBtn').addEventListener('click', function() {
                scrollToNextSection();
            });

            $(document).on('wheel', function(e) {
                if (e.originalEvent.deltaY > 0) {
                    scrollToNextSection();
                } else {
                    scrollToPreviousSection();
                }
            });

            $(document).keydown(function(e) {
                if (e.key === 'ArrowDown') {
                    scrollToNextSection();
                } else if (e.key === 'ArrowUp') {
                    scrollToPreviousSection();
                }
            });

            let touchstartY = 0;
            $(document).on('touchstart', function(e) {
                touchstartY = e.touches[0].clientY;
            });


            $(document).on('touchend', function(e) {
                const touchendY = e.changedTouches[0].clientY;
                const diffY = touchstartY - touchendY;
                if (diffY > 50) {
                    scrollToNextSection();
                } else if (diffY < -50) {
                    scrollToPreviousSection();
                }
            });
        });
    </script>
</body>

</html>
