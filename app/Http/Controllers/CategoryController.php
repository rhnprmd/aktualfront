<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $page_name = "Categories";
        $categories = Category::all();
        $no = 1;
        return view('app.content.categories', compact('page_name', 'categories', 'no'));
    }

    public function store(Request $request)
    {
        $categories = new Category();
        $categories->name = $request->name;
        $saved = $categories->save();

        if ($saved) {
            return redirect()->route('category.index')->with('message', 'Category Added');
        }
        return redirect()->route('category.index')->with('error', 'Fail adding category');
    }

    public function update(Request $request, Category $category)
    {
        $category->name = $request->name;
        $saved = $category->save();

        if ($saved) {
            return redirect()->route('category.index')->with('message', 'Category Updated');
        }
        return redirect()->route('category.index')->with('error', 'Fail updating category');
    }

    public function delete(Category $category)
    {
        if ($category->delete()) {
            return redirect()->route('category.index')->with('message', 'Category Removed');
        }
        return redirect()->route('category.index')->with('error', 'Fail Removing category');
    }
}
