<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostDetail;
use App\Models\PostImage;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $page_name = "Sub Categories";
        $categories = Post::all();
        $no = 1;
        return view('app.content.subcategories', compact('page_name', 'categories', 'no'));
    }

    public function store(Request $request)
    {
        $post = new Post();
        $postdetail = new PostDetail();
        $postimage = new PostImage();

        $postdetail->img_caps = $request->img_caps;
        $postdetail->title = $request->title;
        $postdetail->content = $request->content;
        $postdetail->editor = $request->editor;
        $saved_post_detail = $postdetail->save();

        $post->slug = $request->slug;
        $post->id_category = $request->id_category;
        $post->id_subcategory = $request->id_subcategory;
        $post->id_post = $postdetail->id;
        $saved_post = $post->save();

        $postimage->id_post = $request->id_post;
        $postimage->file = $request->file;
        $postimage->save();

        if ($saved_post) {
            return redirect()->route('post.index')->with('message', 'Post Added');
        }
        return redirect()->route('post.index')->with('error', 'Fail adding post');
    }

    public function update(Request $request, Post $post)
    {
        $post->name = $request->name;
        $saved = $post->save();

        if ($saved) {
            return redirect()->route('post.index')->with('message', 'Post Updated');
        }
        return redirect()->route('post.index')->with('error', 'Fail updating post');
    }

    public function delete(Post $post)
    {
        if ($post->delete()) {
            return redirect()->route('post.index')->with('message', 'Post Removed');
        }
        return redirect()->route('post.index')->with('error', 'Fail Removing post');
    }
}
