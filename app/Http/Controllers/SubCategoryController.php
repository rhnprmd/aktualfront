<?php

namespace App\Http\Controllers;

use App\Models\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $page_name = "Sub Categories";
        $categories = SubCategory::all();
        $no = 1;
        return view('app.content.subcategories', compact('page_name', 'categories', 'no'));
    }

    public function store(Request $request)
    {
        $subcategory = new SubCategory();
        $subcategory->name = $request->name;
        $saved = $subcategory->save();

        if ($saved) {
            return redirect()->route('subcategory.index')->with('message', 'Category Added');
        }
        return redirect()->route('subcategory.index')->with('error', 'Fail adding category');
    }

    public function update(Request $request, SubCategory $subcategory)
    {
        $subcategory->name = $request->name;
        $saved = $subcategory->save();

        if ($saved) {
            return redirect()->route('subcategory.index')->with('message', 'Category Updated');
        }
        return redirect()->route('subcategory.index')->with('error', 'Fail updating category');
    }

    public function delete(SubCategory $subcategory)
    {
        if ($subcategory->delete()) {
            return redirect()->route('subcategory.index')->with('message', 'Category Removed');
        }
        return redirect()->route('subcategory.index')->with('error', 'Fail Removing category');
    }
}
