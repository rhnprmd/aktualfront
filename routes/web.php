<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SubCategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return redirect()->to('login');
});


Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'categories'], function () {
    Route::get('/', [CategoryController::class, 'index'])->name('category.index');
    Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
    Route::put('/update/{category}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('/delete/{category}', [CategoryController::class, 'delete'])->name('category.delete');
});

Route::group(['prefix' => 'subcategories'], function () {
    Route::get('/', [SubCategoryController::class, 'index'])->name('subcategory.index');
    Route::post('/store', [SubCategoryController::class, 'store'])->name('subcategory.store');
    Route::put('/update/{subcategory}', [SubCategoryController::class, 'update'])->name('subcategory.update');
    Route::delete('/delete/{subcategory}', [SubCategoryController::class, 'delete'])->name('subcategory.delete');
});

Route::group(['prefix' => 'post'], function () {
    Route::get('/', [PostController::class, 'index'])->name('post.index');
    Route::post('/store', [PostController::class, 'store'])->name('post.store');
    Route::put('/update/{id}', [PostController::class, 'update'])->name('post.update');
    Route::delete('/delete/{id}', [PostController::class, 'delete'])->name('post.delete');
});
